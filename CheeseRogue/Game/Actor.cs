﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Game
{
    class Actor : IActor
    {
        readonly char Representation;
        public Actor(char Representation)
        {
            this.Representation = Representation;
        }

        public char GetChar()
        {
            return Representation;
        }
    }
}
