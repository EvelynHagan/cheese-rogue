﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Game.Actors
{
    enum CharacterClass
    {
        Warrior = 1,
        Mage = 2,
        Thief = 3
    }
    class Character : Actor
    {
        static readonly char Representation = 'C';
        public Character() : base(Representation) { }

        public string name { get; set; }
        public CharacterClass characterClass { get; set; }
        public Pronouns pronouns { get; set; }


        //     Stats
        int Power { get; set; }
        int Endurance { get; set; }
        int Swiftness { get; set; }
        int Knowledge { get; set; }
        int Spirituality { get; set; }
        int Will { get; set; }

    }
}
