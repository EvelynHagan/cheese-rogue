﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Game.Actors
{
    class Pronouns
    {
        readonly string ssubject;
        readonly string sobject;
        readonly string spossessive_adj;
        readonly string spossessive_pro;
        readonly string sreflexive;
        readonly string descriptor;

        public Pronouns(string isubject, string iobject, string ipossessiveadj, string ipossessivepro, string ireflexive, string idescriptor)
        {
            ssubject = isubject;
            sobject = iobject;
            spossessive_adj = ipossessiveadj;
            spossessive_pro = ipossessivepro;
            sreflexive = ireflexive;
            descriptor = idescriptor;
        }

        public string Fill(string input)
        {
            input = input.Replace("{pronoun-subject}", ssubject);
            input = input.Replace("{pronoun-object}", sobject);
            input = input.Replace("{pronoun-possessiveadj}", spossessive_adj);
            input = input.Replace("{pronoun-possessivepro}", spossessive_pro);
            input = input.Replace("{pronoun-descriptor}", descriptor);
            return input.Replace("{pronoun-reflexive}", sreflexive);
        }
    }

    class NeutralPronouns : Pronouns
    {
        readonly static string ssubject = "they";
        readonly static  string sobject = "them";
        readonly static  string spossessive_adj = "their";
        readonly static  string spossessive_pro = "theirs";
        readonly static  string sreflexive = "themself";
        readonly static string descriptor = "person";
        public NeutralPronouns() : base(ssubject, sobject, spossessive_adj, spossessive_pro, sreflexive, descriptor) { }
    }
    class FemalePronouns : Pronouns
    {
        readonly static string ssubject = "she";
        readonly static string sobject = "her";
        readonly static string spossessive_adj = "hers";
        readonly static string spossessive_pro = "hers";
        readonly static string sreflexive = "herself";
        readonly static string descriptor = "woman";
        public FemalePronouns() : base(ssubject, sobject, spossessive_adj, spossessive_pro, sreflexive, descriptor) { }
    }
    class MalePronouns : Pronouns
    {
        readonly static string ssubject = "he";
        readonly static string sobject = "him";
        readonly static string spossessive_adj = "his";
        readonly static string spossessive_pro = "his";
        readonly static string sreflexive = "himself";
        readonly static string descriptor = "man";
        public MalePronouns() : base(ssubject, sobject, spossessive_adj, spossessive_pro, sreflexive, descriptor) { }
    }


}
