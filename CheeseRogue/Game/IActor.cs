﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Game
{
    interface IActor
    {
        char GetChar();
    }
}
