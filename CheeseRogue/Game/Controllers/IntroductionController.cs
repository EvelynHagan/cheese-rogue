﻿using System.Drawing;
using CheeseRogue.Game.World;
using Colorful;
using Console = Colorful.Console;
namespace CheeseRogue.Game.Controllers
{
    internal class IntroductionController
    {
        private WorldModel world;

        public IntroductionController(WorldModel world)
        {
            this.world = world;
        }

        public void Run()
        {
            Console.Clear();
            
            TextTools.WriteOutTimed("In the land of " + world.world_name + "...", .05f, Color.Yellow, true);
            TextTools.WriteBlankLines(1);
            TextTools.WriteOutTimed("In the dungeon of " + world.dungeon_name + "...", .1f, Color.Yellow, true);
            TextTools.WriteBlankLines(1);
            TextTools.WriteOutTimed("Something stirs...", .2f, Color.Red,true);
            Console.ReadKey();
            
        }

    }
}