﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogue.Game.Actors;
using CheeseRogue.Game.World;
using CheeseRogue.Utilities.DungeonGeneration;

namespace CheeseRogue.Game.Controllers
{
    class GameInstance
    {
        Character pc;
        WorldModel world;
        Dungeon dungeon;
        int seed;
        enum state
        {
            CharacterCreation,
            Introduction,
            InitDungeon,
            Explore,
            Combat
        }
        state State;
        public GameInstance()
        {
            State = state.CharacterCreation;
            pc = new Character();
            world = new WorldModel();
            seed = System.DateTime.Now.Millisecond;
        }
        public void Run()
        {
            while(true)
            {
                switch (State)
                {
                    case state.CharacterCreation:
                        CharacterCreation();
                        break;
                    case state.Introduction:
                        Introduction();
                        break;
                    case state.InitDungeon:
                        InitDungeon();
                        break;
                    default: return;
                }
            }
        }

        private void InitDungeon()
        {
            DungeonInitializer dungeonInitializer = new DungeonInitializer();
            dungeon = dungeonInitializer.InitDungeon();
            State = state.Explore;
        }

        private void Introduction()
        {
            IntroductionController intro = new IntroductionController(world);
            intro.Run();
            State = state.InitDungeon;
        }

        private void CharacterCreation()
        {

            CharacterCreator characterCreator = new CharacterCreator(pc);
            characterCreator.Run();
            State = state.Introduction;
        }
    }
}
