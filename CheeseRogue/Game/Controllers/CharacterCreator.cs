﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogue.Game.Actors;
using Colorful;
using Console = Colorful.Console;
namespace CheeseRogue.Game.Controllers
{
    class CharacterCreator
    {
        Character character_reference;
        public CharacterCreator(Character in_character)
        {
            character_reference = in_character;
        }

        public void Run()
        {
            NameSelection();
            GenderSelection();
            ClassSelection();
            StatSelection();
        }

        private void StatSelection()
        {
            Console.Clear();
            StyleSheet sheet = StyleSheets.Adventure.GetAdventureMagic();
            //Write Game Name
            string header = "Stat Selection";
            header = TextTools.Center(header);
            Console.WriteLine(header, Color.Yellow);
            TextTools.WriteBlankLines(3);

            

            
            int selected = 0;
            int[] stats = new int[] { 10, 10, 10, 10, 10, 10 };
            Color[] option_colors = new Color []{ Color.White, Color.Gray, Color.Gray, Color.Gray, Color.Gray, Color.Gray };
            int point_pool = 10;

            bool done = false;
            while (!done)
            {
                Console.WriteLine(TextTools.PadWithSpaces("Power \t\t- \t" + stats[0], 8), option_colors[0]);
                Console.WriteLine(TextTools.PadWithSpaces("Endurance \t- \t" + stats[1], 8), option_colors[1]);
                Console.WriteLine(TextTools.PadWithSpaces("Swiftness \t- \t" + stats[2], 8), option_colors[2]);
                Console.WriteLine(TextTools.PadWithSpaces("Knowledge \t- \t" + stats[3], 8), option_colors[3]);
                Console.WriteLine(TextTools.PadWithSpaces("Spirituality \t- \t" + stats[4], 8), option_colors[4]);
                Console.WriteLine(TextTools.PadWithSpaces("Will \t\t- \t" + stats[5], 8), option_colors[5]);
                TextTools.WriteBlankLines(1);
                Console.Write(TextTools.PadWithSpaces("Remaining Points: ", 8), Color.Aqua);
                Console.Write(point_pool, Color.Yellow);
                
                TextTools.WriteBlankLines(1);
                Console.Write(TextTools.PadWithSpaces("Controls: ",8), Color.Aqua);
                Console.Write("↑ ↓ → ←", Color.Red);
                
                TextTools.WriteBlankLines(2);
                Console.WriteLine(TextTools.PadWithSpaces("Press enter to continue", 8), Color.Red);
                ConsoleKeyInfo keypress = Console.ReadKey();
                if (keypress.Key == ConsoleKey.UpArrow)
                {
                    selected = Math.Max(0, selected - 1);
                }
                else if (keypress.Key == ConsoleKey.DownArrow)
                {
                    selected = Math.Min(selected+1, 5);
                }
                else if(keypress.Key == ConsoleKey.LeftArrow)
                {
                    if(stats[selected] >5)
                    {
                        point_pool++;
                        stats[selected]--;
                    }
                }
                else if (keypress.Key == ConsoleKey.RightArrow)
                {
                    if (stats[selected] < 20 && point_pool >0)
                    {
                        point_pool--;
                        stats[selected]++;
                    }
                }
                else if(keypress.Key == ConsoleKey.Enter)
                {
                    Console.Write(TextTools.PadWithSpaces("ARE YOU SURE YOU WANT TO CONTINUE? y/n: ",8), Color.Red);
                    ConsoleKeyInfo should_continue = Console.ReadKey();
                    Console.WriteLine();
                    if (should_continue.Key == ConsoleKey.Y)
                        done = true;
                    TextTools.ClearXLines(1);
                }
                option_colors = new Color[] { Color.Gray, Color.Gray, Color.Gray, Color.Gray, Color.Gray, Color.Gray };
                option_colors[selected] = Color.White;

                TextTools.ClearXLines(11);
            }
        }

        private void ClassSelection()
        {
            Console.Clear();
            TextTools.WriteBlankLines(4);
            StyleSheet sheet = StyleSheets.Adventure.GetAdventureMagic();
            //Write Game Name
            string title = "Welcome adventurer to Cheese Rogue";
            title = TextTools.Center(title);

            Console.WriteLineStyled(
                character_reference.pronouns.Fill(
                TextTools.PadWithSpaces("Well now " + character_reference.name
                + " what does a {pronoun-descriptor} like you do for work?", 5)), sheet);

            int class_input = -1;
            Console.WriteLineStyled(TextTools.PadWithSpaces("1 - Warrior", 8), sheet);
            Console.WriteLineStyled(TextTools.PadWithSpaces("2 - Mage", 8), sheet);
            Console.WriteLineStyled(TextTools.PadWithSpaces("3 - Theif", 8), sheet);
            TextTools.WriteBlankLines(2);
            while (class_input == -1)
            {

                int tmp;
                try
                {
                    Console.Write(TextTools.PadWithSpaces("", 5));
                    tmp = Int32.Parse(Console.ReadLine());
                }
                catch
                {
                    tmp = -1;
                }
                if (tmp >= 1 && tmp <= 3)
                {
                    class_input = tmp;
                }
                else
                {
                    TextTools.ClearXLines(2);
                    Console.WriteLineStyled(TextTools.PadWithSpaces("I can't understand you, could you repeat that?", 5), sheet);
                }
            }
            TextTools.ClearXLines(1);
            switch (class_input)
            {
                case 1: character_reference.characterClass = CharacterClass.Warrior;
                    Console.WriteLineStyled(TextTools.PadWithSpaces("Ah so you're a mighty warrior!", 5), sheet);
                    break;
                case 2: character_reference.characterClass = CharacterClass.Mage; 
                    Console.WriteLineStyled(TextTools.PadWithSpaces("Oh you're a scholor of the old ways?", 5), sheet);
                    break;
                case 3: character_reference.characterClass = CharacterClass.Thief;
                    Console.WriteLineStyled(TextTools.PadWithSpaces("Heh, well I'll be sure to watch my pockets then.", 5), sheet);
                    break;
            }

            Console.WriteLine(TextTools.PadWithSpaces("Press any key to continue", 5), Color.LightGray);
            Console.ReadKey();
        }

        private void GenderSelection()
        {
            Console.Clear();
            TextTools.WriteBlankLines(4);
            StyleSheet sheet = StyleSheets.Adventure.GetAdventureMagic();
            //Write Game Name
            string title = "Welcome adventurer to Cheese Rogue";
            title = TextTools.Center(title);
            Console.WriteLineStyled(title, StyleSheets.Adventure.GetAdventureBold());
            TextTools.WriteBlankLines(1);
            Console.WriteLineStyled(TextTools.PadWithSpaces("Now tell me " + character_reference.name
                + " my glasses are foggy.",5), sheet);
            System.Threading.Thread.Sleep(500);
            Console.WriteLineStyled(TextTools.PadWithSpaces("Are you a boy or a girl?", 5), sheet);

            int gender_input = -1;
            Console.WriteLineStyled(TextTools.PadWithSpaces("1 - Boy", 8), sheet);
            Console.WriteLineStyled(TextTools.PadWithSpaces("2 - Girl", 8), sheet);
            Console.WriteLineStyled(TextTools.PadWithSpaces("3 - Neither", 8), sheet);
            TextTools.WriteBlankLines(2);
            while (gender_input == -1)
            {
                
                int tmp;
                try
                {
                    Console.Write(TextTools.PadWithSpaces("", 5));
                    tmp = Int32.Parse(Console.ReadLine());
                }
                catch
                {
                    tmp = -1;
                }
                if(tmp >=1 && tmp <=3)
                {
                    gender_input = tmp;
                }
                else
                {
                    TextTools.ClearXLines(2);
                    Console.WriteLineStyled(TextTools.PadWithSpaces("I can't understand you, could you repeat that?", 5), sheet);
                }
            }
            TextTools.ClearXLines(1);
            switch (gender_input)
            {
                case 1: character_reference.pronouns = new MalePronouns(); break;
                case 2: character_reference.pronouns = new FemalePronouns(); break;
                case 3: character_reference.pronouns = new NeutralPronouns(); break;
            }

            if (gender_input == 3)
            {
                Console.WriteLineStyled(TextTools.PadWithSpaces("... Oh... Ok... Well good to know!", 5), sheet);
            }
            else
            {
                string saying_string = "Ah so you are a {pronoun-descriptor}!";
                saying_string = character_reference.pronouns.Fill(saying_string);
                Console.WriteLineStyled(TextTools.PadWithSpaces(saying_string, 5), sheet);
            }

            Console.WriteLine(TextTools.PadWithSpaces("Press any key to continue", 5), Color.LightGray);
            Console.ReadKey();
        }

        private void NameSelection()
        {
            Console.Clear();
            TextTools.WriteBlankLines(4);

            //Write Game Name
            string title = "Welcome adventurer to Cheese Rogue";
            title = TextTools.Center(title);
            Console.WriteLineStyled(title,StyleSheets.Adventure.GetAdventureBold());
            Console.Write("\n"+TextTools.PadWithSpaces("And what might be your name? ", 5), Color.Yellow);
            character_reference.name = Console.ReadLine();
            TextTools.WriteBlankLines(1);
            Console.Write(TextTools.PadWithSpaces("",5));
            TextTools.WriteOutTimed("...........", .1f, false);
            TextTools.WriteBlankLines(1);
            Console.WriteStyled(TextTools.PadWithSpaces("you don't say?", 5), StyleSheets.Adventure.GetAdventureMagic());
            System.Threading.Thread.Sleep(500);
            TextTools.WriteBlankLines(1);
            Console.WriteStyled(TextTools.PadWithSpaces("I once heard a legend about a " + character_reference.name, 5) + ".", StyleSheets.Adventure.GetAdventureMagic());
            TextTools.WriteBlankLines(3);
            Console.WriteLine(TextTools.PadWithSpaces("Press any key to continue", 5), Color.LightGray);
            Console.ReadKey();
        }
    }
}
