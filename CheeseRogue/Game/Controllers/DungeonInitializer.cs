﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogue.Utilities.DungeonGeneration;
using CheeseRogue.Utilities.FloorGeneration;

namespace CheeseRogue.Game.Controllers
{
    class DungeonInitializer
    {
        Dungeon dungeon;
        int current_floor = -1;
        DungeonGenerator gen;
        public DungeonInitializer()
        {
            gen = new DungeonGenerator();
            dungeon = new Dungeon();
            
        }
        public Dungeon InitDungeon()
        {
            dungeon = gen.GenerateDungeon(dungeon);
            return dungeon;
        }


        
    }
}
