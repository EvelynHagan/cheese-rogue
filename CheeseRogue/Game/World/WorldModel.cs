﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace CheeseRogue.Game.World
{
    class WorldModel
    {
        public string world_name { get; set; }
        public string dungeon_name { get; set; }
        public string boss_name { get; set; }

        public WorldModel()
        {
            Random random = new Random();
            string filepath = Environment.CurrentDirectory + "/Resources/WorldGenData/";
            using(StreamReader sr = new StreamReader(filepath + "DungeonNames.txt"))
            {
                string tmp = sr.ReadToEnd();
                tmp = tmp.Replace("\r", "");
                string[] array = tmp.Split('\n');
                dungeon_name = array[random.Next(array.Length)];
            }
            using (StreamReader sr = new StreamReader(filepath + "WorldNames.txt"))
            {
                string tmp = sr.ReadToEnd();
                tmp = tmp.Replace("\r", "");
                string[] array = tmp.Split('\n');
                world_name = array[random.Next(array.Length)];
            }
        }
    }
}
