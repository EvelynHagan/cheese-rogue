﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogue.Game;
using CheeseRogue.Game.Controllers;
using CheeseRogue.Utilities;

namespace CheeseRogue
{
    enum state
    {
        MainMenu = 1,
        GameMenu,
        LoadMenu,
        OptionsMenu,
        EXIT,
        DEBUG,
        INVALID
        
    }

    
    class GameBase
    {
        state State;
        public GameBase()
        {
            State = state.MainMenu;
        }
        bool exit = false;
        public int Run()
        {
            
            while(!exit)
            {
                switch(State)
                {
                    case state.MainMenu:
                        State = MainMenu();
                        
                        break;
                    case state.GameMenu:
                        State = GameMenu();
                        break;
                    case state.DEBUG:
                        State = DebugMenu();
                        break;
                    default:
                        exit = true;
                        break;
                }
            }
            return 1;
        }

        private state DebugMenu()
        {
            DebugInstance instance = new DebugInstance();
            instance.Run();
            return state.MainMenu;
        }

        state MainMenu()
        {
            
            string selectinfo = "";
            int value = -1;
            while (value == -1) {
                Console.Clear();
                TextTools.WriteBlankLines(3);
                TextTools.WriteCentered("Welcome to CheeseRogue", Color.Red);
                TextTools.WriteBlankLines(4);
                TextTools.WriteCentered("1 - Start a new game");
                TextTools.WriteCentered("2 - Load a game");
                TextTools.WriteCentered("3 - Options Menu ");
                TextTools.WriteCentered("4 - Exit");
                TextTools.WriteBlankLines(4);
                TextTools.WriteCentered(selectinfo);

                try
                {
                    value = int.Parse(Console.ReadLine());
                }
                catch
                {
                    value = -1;
                }
                if ((value < 1 || value > 4) && value != 666)
                {
                    selectinfo = "Invalid Option, try again.";
                    value = -1;
                }
            }
            switch(value)
            {
                case 1: return state.GameMenu;
                case 2: return state.LoadMenu;
                case 3: return state.OptionsMenu;
                case 4: return state.EXIT;
                case 666:
#if DEBUG
                    return state.DEBUG;
#endif
                default: return state.INVALID;
            }
        }

        state GameMenu()
        {
            GameInstance instance = new GameInstance();
            instance.Run();
            return state.MainMenu;
        }
    }
}
