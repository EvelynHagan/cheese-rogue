﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Colorful;

namespace CheeseRogue.StyleSheets
{
    class Adventure
    {
        public static StyleSheet GetAdventureBold()
        {
            StyleSheet sheet = new StyleSheet(Color.GhostWhite);
            sheet.AddStyle("Cheese Rogue", Color.Red, match => match.ToUpper());
            return sheet;
        }

        public static StyleSheet GetAdventureMagic()
        {
            StyleSheet sheet = new StyleSheet(Color.GhostWhite);
            sheet.AddStyle("say", Color.SteelBlue);
            sheet.AddStyle("\\?", Color.Plum);
            sheet.AddStyle("legend", Color.Purple);
            return sheet;
        }
    }
}
