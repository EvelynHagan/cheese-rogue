﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration;

namespace CheeseRogue.Utilities
{
    class DebugInstance
    {
        public void Run()
        {
            bool done = false;
            while(!done)
            {
                string selectinfo = "";
                int value = -1;
                while (value == -1)
                {
                    Console.Clear();
                    TextTools.WriteBlankLines(3);
                    TextTools.WriteCentered("Welcome to CheeseRogue", Color.Red);
                    TextTools.WriteBlankLines(4);
                    TextTools.WriteCentered("1 - See Level Generator");
                    TextTools.WriteCentered("2 - See Level Viewer");
                    TextTools.WriteCentered("99 - Return");
                    TextTools.WriteBlankLines(4);
                    TextTools.WriteCentered(selectinfo);

                    try
                    {
                        value = int.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        value = -1;
                    }
                    if ((value < 1 || value > 2) && value != 99)
                    {
                        selectinfo = "Invalid Option, try again.";
                        value = -1;
                    }
                }
                switch (value)
                {
                    case 1: LevelGenerationMenu();break;
                    case 2: LevelViewerTest();break;
                    default: done = true;break;
                }
            }
        }

        private void LevelViewerTest()
        {
            FloorViewer.FloorViewController floorViewController = new FloorViewer.FloorViewController();
            floorViewController.Run();
        }

        private void LevelGenerationMenu()
        {
            FloorGenerator generator = new FloorGenerator();
            FloorGeneratorConfig config = new FloorGeneratorConfig();

            config.deadend_passes = 100;
            config.EmptyAfterHallwayPercent = .02f;
            Random random = new Random();
            config.MapSizeX = random.Next(80, 150);
            config.MapSizeY = random.Next(50, 100);
            config.RoomMinHeight = random.Next(3, 7);
            config.RoomMinWidth = config.RoomMinHeight;
            int maxsize = 10;
            if (random.Next(100) > 75)
                maxsize = 20;
            config.RoomMaxHeight = random.Next(config.RoomMinHeight + 1, maxsize);
            config.RoomMinWidth = config.RoomMaxHeight;
            config.RoomPadding = random.Next(1, 12);
            config.hallway_gen_type = FloorGeneration.HallwayGeneration.HallwayTypes.RecursiveBacktrack;
            config.RoomPlacementAttempts = random.Next(200, 3000);

            Floor floor = generator.generate_floor(config);
            floor.DebugDraw();
        }
    }
}
