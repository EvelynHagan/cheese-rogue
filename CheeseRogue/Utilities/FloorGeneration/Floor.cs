﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration
{
    class Floor : IFloor
    {
        FloorGeneratorConfig config;

        public char[,] floor_map;
        public Floor()
        {
            config = new FloorGeneratorConfig();
        }
        public Floor(FloorGeneratorConfig config)
        {
            this.config = config;
        }

        public void DebugDraw()
        {
            Console.Clear();
            for(int y = 0; y < floor_map.GetLength(1); y++)
            {
                for(int x=0;x<floor_map.GetLength(0); x++)
                {
                    if (floor_map[x, y] == ' ')
                        Console.Write('█');
                    else
                        Console.Write('*');
                }
                TextTools.WriteBlankLines(1);
            }
            Console.ReadKey();
        }
    }
}
