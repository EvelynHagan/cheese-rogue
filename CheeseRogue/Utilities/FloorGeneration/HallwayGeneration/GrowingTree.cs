﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.HallwayGeneration
{
    class GrowingTree : IHallwayGenerator
    {
        Random random;
        public char[,] GenerateHallways(char[,] floor_map, FloorGeneratorConfig config, Random random)
        {
            this.random = random;
            char count = 'a';
            while (MapHallwaysTooEmpty(floor_map, config))
            {
                //count++;
                //Growing Tree
                int x = random.Next(0, config.MapSizeX - 1);
                int y = random.Next(0, config.MapSizeY - 1);
                while (floor_map[x, y] != ' ' || !cell_is_valid(new cell(x, y), floor_map, 0))
                {
                    x = random.Next(0, config.MapSizeX - 1);
                    y = random.Next(0, config.MapSizeY - 1);
                }
                List<cell> OpenList = new List<cell>();
                OpenList.Add(new cell(x, y));
                floor_map[x, y] = (char)count;
                while (OpenList.Count > 0)
                {
                    int curr_indx;
                    if (random.Next(0, 100) < 80)
                        curr_indx = random.Next(0, OpenList.Count);
                    else if (random.Next(0, 100) < 50)
                        curr_indx = 0;
                    else curr_indx = OpenList.Count - 1;
                    cell current_cell = OpenList[curr_indx];

                    List<cell> shuffle_list = new List<cell>();
                    if (current_cell.x > 0)
                        shuffle_list.Add(new cell(current_cell.x - 1, current_cell.y));
                    if (current_cell.x < floor_map.GetLength(0))
                        shuffle_list.Add(new cell(current_cell.x + 1, current_cell.y));
                    if (current_cell.y > 0)
                        shuffle_list.Add(new cell(current_cell.x, current_cell.y - 1));
                    if (current_cell.y < floor_map.GetLength(1))
                        shuffle_list.Add(new cell(current_cell.x, current_cell.y + 1));
                    bool shuffle_list_invalid = true;
                    if (shuffle_list.Count != 0)
                    {
                        shuffle_list = shuffle_list.OrderBy((item) => random.Next()).ToList<cell>();
                        for (int i = 0; i < shuffle_list.Count; i++)
                        {
                            if (cell_is_valid(shuffle_list[i], floor_map, 1))
                            {
                                shuffle_list_invalid = false;
                                OpenList.Add(shuffle_list[i]);
                                floor_map[shuffle_list[i].x, shuffle_list[i].y] = (char)count;
                                break;
                            }
                        }
                    }
                    else shuffle_list.RemoveAt(curr_indx);
                    if (shuffle_list_invalid)
                        OpenList.RemoveAt(curr_indx);
                }
                count++;
            }

            return floor_map;
        }

        bool MapHallwaysTooEmpty(char[,] floor_map, FloorGeneratorConfig config)
        {
            float count = 0;
            for (int x = 0; x < floor_map.GetLength(0); x++)
            {
                for (int y = 0; y < floor_map.GetLength(1); y++)
                {
                    if (cell_is_valid(new cell(x, y), floor_map, 0))
                        count++;
                }
            }
            float cells = floor_map.GetLength(0) * floor_map.GetLength(1);
            return (count / cells) > config.EmptyAfterHallwayPercent;
        }

        bool cell_is_valid(cell icell, char[,] floor_map, int target)
        {
            int N = 0, S = 0, E = 0, W = 0;
            int Empty = 0;
            if (icell.x <= 0 || floor_map[icell.x - 1, icell.y] != ' ')
                W = 1;
            if (icell.x >= floor_map.GetLength(0) - 1 || floor_map[icell.x + 1, icell.y] != ' ')
                E = 1;
            if (icell.y <= 0 || floor_map[icell.x, icell.y - 1] != ' ')
                N = 1;
            if (icell.y >= floor_map.GetLength(1) - 1 || floor_map[icell.x, icell.y + 1] != ' ')
                S = 1;
            if (floor_map[icell.x, icell.y] != ' ')
                Empty = 1;

            return Empty == 0 && (N + S + E + W == target);
        }

    }
}
