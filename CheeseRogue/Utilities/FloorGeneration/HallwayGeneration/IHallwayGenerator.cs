﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.HallwayGeneration
{
    interface IHallwayGenerator
    {
        char[,] GenerateHallways(char[,] floor_map, FloorGeneratorConfig config, Random random);
    }
}
