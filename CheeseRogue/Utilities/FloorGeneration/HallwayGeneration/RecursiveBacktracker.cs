﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.HallwayGeneration
{
    class RecursiveBacktracker : IHallwayGenerator
    {
        char count;
        Random random;
        public char[,] GenerateHallways(char[,] floor_map, FloorGeneratorConfig config, Random random)
        {
            this.random = random;
            count = 'a';
            while (MapHallwaysTooEmpty(floor_map, config))
            {
                int x = random.Next(0, config.MapSizeX - 1);
                int y = random.Next(0, config.MapSizeY - 1);
                while (floor_map[x, y] != ' ' || !cell_is_valid(new cell(x, y), floor_map, 0))
                {
                    x = random.Next(0, config.MapSizeX - 1);
                    y = random.Next(0, config.MapSizeY - 1);
                }
                recursiveBackTrack(floor_map, new cell(x, y));
                Floor tmp = new Floor();
                tmp.floor_map = floor_map;
                count++;
            }
            return floor_map;
        }

        void recursiveBackTrack(char[,] floor_map, cell curr_cell)
        {
            floor_map[curr_cell.x, curr_cell.y] = count;
            List<cell> neighbors = GetNeighbors(floor_map, curr_cell);
            IOrderedEnumerable<cell> shuffled = neighbors.OrderBy((item) => random.Next());
            foreach(cell neighbor in shuffled)
            {
                if (cell_is_valid(neighbor, floor_map, 1))
                    recursiveBackTrack(floor_map,neighbor);
            }
        }

        List<cell> GetNeighbors(char[,] floor_map, cell curr_cell)
        {
            List<cell> retval = new List<cell>();
            if (curr_cell.x > 0 && floor_map[curr_cell.x - 1, curr_cell.y] == ' ')
                retval.Add(new cell(curr_cell.x - 1, curr_cell.y));
            if (curr_cell.y > 0 && floor_map[curr_cell.x , curr_cell.y - 1] == ' ')
                retval.Add(new cell(curr_cell.x, curr_cell.y - 1));
            if (curr_cell.x < floor_map.GetLength(0) && floor_map[curr_cell.x + 1, curr_cell.y] == ' ')
                retval.Add(new cell(curr_cell.x + 1, curr_cell.y));
            if (curr_cell.y < floor_map.GetLength(1) && floor_map[curr_cell.x, curr_cell.y + 1] == ' ')
                retval.Add(new cell(curr_cell.x, curr_cell.y + 1));

            return retval;

        }

        bool MapHallwaysTooEmpty(char[,] floor_map, FloorGeneratorConfig config)
        {
            float count = 0;
            for (int x = 0; x < floor_map.GetLength(0); x++)
            {
                for (int y = 0; y < floor_map.GetLength(1); y++)
                {
                    if (cell_is_valid(new cell(x, y), floor_map, 0))
                        count++;
                }
            }
            float cells = floor_map.GetLength(0) * floor_map.GetLength(1);
            return (count / cells) > config.EmptyAfterHallwayPercent;
        }

        bool cell_is_valid(cell icell, char[,] floor_map, int target)
        {
            int N = 0, S = 0, E = 0, W = 0;
            int Empty = 0;
            if (icell.x <= 0 || floor_map[icell.x - 1, icell.y] != ' ')
                W = 1;
            if (icell.x >= floor_map.GetLength(0) - 1 || floor_map[icell.x + 1, icell.y] != ' ')
                E = 1;
            if (icell.y <= 0 || floor_map[icell.x, icell.y - 1] != ' ')
                N = 1;
            if (icell.y >= floor_map.GetLength(1) - 1 || floor_map[icell.x, icell.y + 1] != ' ')
                S = 1;
            if (floor_map[icell.x, icell.y] != ' ')
                Empty = 1;

            return Empty == 0 && (N + S + E + W <= target);
        }

    }
}
