﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration
{
    static class LevelGenerationConstants
    {
        public static readonly char room_edge = 'X';
        public static readonly char room_middle = 'O';
        public static readonly char connection = '-';
        public static readonly int connection_integer = -2;
        public static readonly int already_connected_integer = -3;
        public static readonly int room_domain = 500;
        public static readonly int corridor_domain = 1;
    }
}
