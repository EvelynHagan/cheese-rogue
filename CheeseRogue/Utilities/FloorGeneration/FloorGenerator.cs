﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration.ConnectionGeneration;

namespace CheeseRogue.Utilities.FloorGeneration
{
    public class FloorGeneratorConfig
    {
        
        public FloorGeneratorConfig()
        {
            MapSizeX = 150;
            MapSizeY = 150;

            RoomPlacementAttempts = 100000;
            RoomMinWidth = 3;
            RoomMaxWidth = 20;
            RoomMinHeight = 3;
            RoomMaxHeight = 20;

            RoomPadding = 8;

            EmptyAfterHallwayPercent = .05f;

            deadend_passes = 100;

            room_gen_type = RoomGeneration.RoomGenTypes.RandomSquarePlacementGenerator;
            hallway_gen_type = HallwayGeneration.HallwayTypes.RecursiveBacktrack;
            connection_gen_types = ConnectionTypes.Standard;
            cleanup_types = new List<Cleanup.CleanupTypes>
            {
                Cleanup.CleanupTypes.ClearDeadends,
                Cleanup.CleanupTypes.x2Scale
            };
        }
        public Random random;
        public int seed { get; set; }
        #region Floor Info
        public int MapSizeX { get; set; }
        public int MapSizeY { get; set; }
        #endregion
        #region Room Variables
        public int RoomPlacementAttempts { get; set; }

        public int RoomMinWidth { get; set; }
        public int RoomMaxWidth { get; set; }
        public int RoomMinHeight { get; set; }
        public int RoomMaxHeight { get; set; }

        public int RoomPadding { get; set; }
        #endregion

        #region Hallway
        public float EmptyAfterHallwayPercent;
        #endregion

        #region Cleanup
        public int deadend_passes = 0;
        #endregion

        #region GenerationTypes
        public RoomGeneration.RoomGenTypes room_gen_type;
        public HallwayGeneration.HallwayTypes hallway_gen_type;
        public ConnectionGeneration.ConnectionTypes connection_gen_types;
        public List<Cleanup.CleanupTypes> cleanup_types;
        #endregion

    }
    class FloorGenerator
    {
        
        
        int seed = -1;
        Random random;
        public Floor generate_floor(FloorGeneratorConfig config)
        {

            if(seed==-1)
            {
                seed = System.DateTime.Now.Millisecond;
            }
            random = new Random(seed);
            config.random = random;
            config.seed = seed;

            Floor floor = new Floor(config);

            char[,] floor_map = new char[config.MapSizeX, config.MapSizeY];
            for(int x = 0; x < config.MapSizeX; x++)
            {
                for(int y = 0; y < config.MapSizeY; y++)
                {
                    floor_map[x, y] = ' ';
                }
            }
            floor.floor_map = floor_map;
            PlaceRooms(floor_map,config);
            //floor.DebugDraw();
            GenerateHallways(floor_map, config);
            //floor.DebugDraw();
            int[,] region_map = new int[config.MapSizeX, config.MapSizeY];
            GenerateConnections(floor_map, region_map, config);
            //floor.DebugDraw();
            floor_map = CleanupPasses(floor_map, config);
            floor.floor_map = floor_map;
            return floor;
        }

        public Floor generate_floor(FloorGeneratorConfig config, int seed)
        {
            this.seed = seed;
            return generate_floor(config);
        }
        #region Rooms
        char[,] PlaceRooms(char[,] floor_map, FloorGeneratorConfig config)
        {
            RoomGeneration.IRoomGenerator gen = new RoomGeneration.RandomSquarePlacementGenerator();
            switch (config.room_gen_type)
            {
                case RoomGeneration.RoomGenTypes.RandomSquarePlacementGenerator:
                    gen = new RoomGeneration.RandomSquarePlacementGenerator();
                    break;
            }
            return gen.PlaceRooms(floor_map, config, random);
        }
        #endregion 

        #region Hallways
        private char[,] GenerateHallways(char[,] floor_map, FloorGeneratorConfig config)
        {
            HallwayGeneration.IHallwayGenerator gen = new HallwayGeneration.GrowingTree();
            switch (config.hallway_gen_type)
            {
                case HallwayGeneration.HallwayTypes.GrowingTree:
                    gen = new HallwayGeneration.GrowingTree();
                    break;
                case HallwayGeneration.HallwayTypes.RecursiveBacktrack:
                    gen = new HallwayGeneration.RecursiveBacktracker();
                    break;
            }
            return gen.GenerateHallways(floor_map, config, random);
        }
        #endregion

        #region Connections
        private char[,] GenerateConnections(char[,] floor_map, int[,] region_map, FloorGeneratorConfig config)
        {
            IConnectionGenerator gen = new StandardConnections();
            switch(config.connection_gen_types)
            {
                case ConnectionTypes.Standard:
                    gen = new StandardConnections();
                    break;
            }
            return gen.GenerateConnections(floor_map, region_map, config, random);
        }
        #endregion

        #region Cleanup
        private char[,] CleanupPasses(char[,] floor_map, FloorGeneratorConfig config)
        {
            Cleanup.ICleanupPass pass;
            foreach (Cleanup.CleanupTypes type in config.cleanup_types)
            {
                switch(type)
                {
                    case Cleanup.CleanupTypes.ClearDeadends:
                        pass = new Cleanup.CleanupDeadends();
                        floor_map = pass.Run(ref floor_map, config, random);
                        break;
                    case Cleanup.CleanupTypes.x2Scale:
                        pass = new Cleanup.x2Scale();
                        floor_map = pass.Run(ref floor_map, config, random);
                        break;
                }
            }
            return floor_map;
        }
        #endregion
    }
}
