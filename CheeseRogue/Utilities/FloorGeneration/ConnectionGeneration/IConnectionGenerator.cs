﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.ConnectionGeneration
{
    interface IConnectionGenerator
    {
        char[,] GenerateConnections(char[,] floor_map, int[,] region_map, FloorGeneratorConfig config, Random random);
    }
}
