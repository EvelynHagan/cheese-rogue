﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheeseRogue.Utilities.FloorGeneration.ConnectionGeneration
{
    public class StandardConnections : IConnectionGenerator
    {
        Random random;
        public char[,] GenerateConnections(char[,] floor_map, int[,] region_map, FloorGeneratorConfig config, Random random)
        {
            this.random = random;
            Dictionary<int, bool> region_connection_mapping = GenerateRegionMap(floor_map, region_map);
            MarkConnections(region_map);
            CompleteConnections(region_map, floor_map, region_connection_mapping);
            return floor_map;
        }
        private Dictionary<int, bool> GenerateRegionMap(char[,] floor_map, int[,] region_map)
        {
            Dictionary<int, bool> region_connection_mapping = new Dictionary<int, bool>();
            Dictionary<string, int> region_mapping = new Dictionary<string, int>();
            int region_count = 1;
            for (int x = 0; x < floor_map.GetLength(0); x++)
            {
                for (int y = 0; y < floor_map.GetLength(1); y++)
                {
                    region_map[x, y] = -1;
                }
            }
            for (int x = 0; x < floor_map.GetLength(0); x++)
            {
                for (int y = 0; y < floor_map.GetLength(1); y++)
                {

                    if (floor_map[x, y] == LevelGenerationConstants.room_edge)
                    {
                        region_mapping.Add('R' + region_count.ToString(), LevelGenerationConstants.room_domain + region_count);
                        int room_region = LevelGenerationConstants.room_domain + region_count;
                        region_count++;

                        int startcol = x;
                        int currcol = x;
                        int currrow = y;
                        while (floor_map[startcol, currrow] == LevelGenerationConstants.room_edge)
                        {
                            currcol = startcol;
                            while (floor_map[currcol, currrow] == LevelGenerationConstants.room_edge || floor_map[currcol, currrow] == LevelGenerationConstants.room_middle)
                            {
                                floor_map[currcol, currrow] = ' ';
                                region_map[currcol, currrow] = room_region;
                                currcol++;
                            }
                            currrow++;
                        }
                    }
                    else if (floor_map[x, y] != ' ')
                    {
                        if (!region_mapping.ContainsKey(floor_map[x, y].ToString()))
                        {
                            region_mapping.Add(floor_map[x, y].ToString(), LevelGenerationConstants.corridor_domain + region_count);
                            region_count++;
                        }
                        region_map[x, y] = region_mapping[floor_map[x, y].ToString()];
                    }
                }
            }
            foreach (int key in region_mapping.Values)
            {
                region_connection_mapping.Add(key, false);
            }
            return region_connection_mapping;
        }
        int[,] MarkConnections(int[,] region_map)
        {
            for (int x = 0; x < region_map.GetLength(0); x++)
            {
                for (int y = 0; y < region_map.GetLength(1); y++)
                {
                    if (cellIsConnection(new cell(x, y), region_map))
                        region_map[x, y] = LevelGenerationConstants.connection_integer;
                }
            }
            return region_map;
        }
        bool cellIsConnection(cell in_cell, int[,] region_map)
        {
            bool valid = false;

            if (region_map[in_cell.x, in_cell.y] == -1)
            {
                Dictionary<int, int> hs = new Dictionary<int, int>();
                if (in_cell.x > 0)
                {
                    int key = region_map[in_cell.x - 1, in_cell.y];
                    if (hs.ContainsKey(key))
                    {
                        hs[key]++;
                    }
                    else
                        hs.Add(key, 1);
                }
                if (in_cell.x < region_map.GetLength(0) - 1)
                {
                    int key = region_map[in_cell.x + 1, in_cell.y];
                    if (hs.ContainsKey(key))
                    {
                        hs[key]++;
                    }
                    else
                        hs.Add(key, 1);
                }
                if (in_cell.y > 0)
                {
                    int key = region_map[in_cell.x, in_cell.y - 1];
                    if (hs.ContainsKey(key))
                    {
                        hs[key]++;
                    }
                    else
                        hs.Add(key, 1);
                }
                if (in_cell.y < region_map.GetLength(1) - 1)
                {
                    int key = region_map[in_cell.x, in_cell.y + 1];
                    if (hs.ContainsKey(key))
                    {
                        hs[key]++;
                    }
                    else
                        hs.Add(key, 1);
                }
                if (hs.ContainsKey(-1))
                    hs.Remove(-1);
                if (hs.ContainsKey(LevelGenerationConstants.connection_integer))
                    hs.Remove(LevelGenerationConstants.connection_integer);
                if (hs.Keys.Count > 1)
                    valid = true;
            }


            return valid;
        }

        void CompleteConnections(int[,] region_map, char[,] floor_map, Dictionary<int, bool> connection_valid_mapping)
        {
            List<int> regions_to_process = new List<int>(); ;
            int selected_region = -1;
            while (selected_region == -1)
            {
                int x = random.Next(0, region_map.GetLength(0));
                int y = random.Next(0, region_map.GetLength(1));
                if (region_map[x, y] >= LevelGenerationConstants.room_domain)
                    selected_region = region_map[x, y];
            }
            regions_to_process.Add(selected_region);
            int count = 0;
            while (connection_valid_mapping.Values.Contains(false) && count < 10000 && regions_to_process.Count > 0)
            {
                regions_to_process = regions_to_process.OrderBy((item) => random.Next()).ToList<int>();
                selected_region = regions_to_process[0];
                List<cell> cells = GetCellsConnectedToRegion(region_map, selected_region);
                cells = cells.OrderBy((item) => random.Next()).ToList<cell>();
                if (cells.Count == 0)
                {
                    connection_valid_mapping[selected_region] = true;
                    regions_to_process.Remove(selected_region);
                    count++;
                    continue;
                }
                cell target = cells[0];
                region_map[target.x, target.y] = LevelGenerationConstants.already_connected_integer;
                floor_map[target.x, target.y] = LevelGenerationConstants.room_edge;

                List<int> connections = getRegionsConnectedToCell(region_map, target);
                connections.Remove(selected_region);
                for (int i = 0; i < connections.Count; i++)
                {

                    if (!connection_valid_mapping[connections[i]])
                        regions_to_process.Add(connections[i]);
                }
                FillRegion(region_map, floor_map, selected_region);
                connection_valid_mapping[selected_region] = true;

                count++;
            }
            for (int x = 0; x < region_map.GetLength(0); x++)
            {
                for (int y = 0; y < region_map.GetLength(1); y++)
                {
                    if (region_map[x, y] > LevelGenerationConstants.room_domain)
                        floor_map[x, y] = LevelGenerationConstants.room_edge;
                }
            }
        }

        void FillRegion(int[,] region_map, char[,] floor_map, int region)
        {
            for (int x = 0; x < region_map.GetLength(0); x++)
            {
                for (int y = 0; y < region_map.GetLength(1); y++)
                {
                    if (region_map[x, y] == region)
                    {
                        //region_map[x, y] = LevelGenerationConstants.already_connected_integer;
                        floor_map[x, y] = LevelGenerationConstants.room_edge;
                    }
                }
            }
        }

        List<cell> GetCellsConnectedToRegion(int[,] region_map, int region)
        {
            List<cell> connected_cells = new List<cell>();
            for (int x = 0; x < region_map.GetLength(0); x++)
            {
                for (int y = 0; y < region_map.GetLength(1); y++)
                {
                    if (region_map[x, y] == LevelGenerationConstants.connection_integer && cellIsConnectionToRegion(new cell(x, y), region_map, region))
                        connected_cells.Add(new cell(x, y));
                }
            }
            return connected_cells;
        }
        bool cellIsConnectionToRegion(cell in_cell, int[,] region_map, int region)
        {
            if (in_cell.x > 0)
            {
                if (region_map[in_cell.x - 1, in_cell.y] == region)
                    return true;

            }
            if (in_cell.x < region_map.GetLength(0) - 1)
            {
                if (region_map[in_cell.x + 1, in_cell.y] == region)
                    return true;
            }
            if (in_cell.y > 0)
            {
                if (region_map[in_cell.x, in_cell.y - 1] == region)
                    return true;
            }
            if (in_cell.y < region_map.GetLength(1) - 1)
            {
                if (region_map[in_cell.x, in_cell.y + 1] == region)
                    return true;
            }



            return false;
        }
        List<int> getRegionsConnectedToCell(int[,] region_map, cell in_cell)
        {
            List<int> connected_regions = new List<int>();
            if (in_cell.x > 0)
            {
                if (!connected_regions.Contains(region_map[in_cell.x - 1, in_cell.y]))
                    connected_regions.Add(region_map[in_cell.x - 1, in_cell.y]);

            }
            if (in_cell.x < region_map.GetLength(0) - 1)
            {
                if (!connected_regions.Contains(region_map[in_cell.x + 1, in_cell.y]))
                    connected_regions.Add(region_map[in_cell.x + 1, in_cell.y]);
            }
            if (in_cell.y > 0)
            {
                if (!connected_regions.Contains(region_map[in_cell.x, in_cell.y - 1]))
                    connected_regions.Add(region_map[in_cell.x, in_cell.y - 1]);
            }
            if (in_cell.y < region_map.GetLength(1) - 1)
            {
                if (!connected_regions.Contains(region_map[in_cell.x, in_cell.y + 1]))
                    connected_regions.Add(region_map[in_cell.x, in_cell.y + 1]);
            }
            connected_regions.Remove(-1);
            connected_regions.Remove(-2);
            connected_regions.Remove(-3);
            return connected_regions;
        }
    }
}
