﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.RoomGeneration
{
    class RandomSquarePlacementGenerator : IRoomGenerator
    {
        Random random;
        public char[,] PlaceRooms(char[,] floor_map, FloorGeneratorConfig config, Random random)
        {
            this.random = random;
            for (int i = 0; i < config.RoomPlacementAttempts; i++)
            {
                int target_room_width = random.Next(config.RoomMinWidth, config.RoomMaxWidth + 1);
                int target_room_height = random.Next(config.RoomMinHeight, config.RoomMaxHeight + 1);

                floor_map = TryPlaceRoom(target_room_width, target_room_height, config.RoomPadding, floor_map);
            }
            return floor_map;
        }

        private char[,] TryPlaceRoom(int target_room_width, int target_room_height, int room_padding, char[,] floor_map)
        {
            int upper_left_x = random.Next(room_padding, floor_map.GetLength(0) - target_room_width - room_padding + 1);
            int upper_left_y = random.Next(room_padding, floor_map.GetLength(1) - target_room_height - room_padding + 1);
            if (RoomCanBePlaced(target_room_width, target_room_height, upper_left_x, upper_left_y, room_padding, floor_map))
            {
                for (int x = upper_left_x; x < upper_left_x + target_room_width; x++)
                {
                    for (int y = upper_left_y; y < upper_left_y + target_room_height; y++)
                    {
                        if (x == upper_left_x || x == upper_left_x + target_room_width - 1
                            || y == upper_left_y || y == upper_left_y + target_room_height - 1)
                        {
                            floor_map[x, y] = LevelGenerationConstants.room_edge;
                        }
                        else floor_map[x, y] = LevelGenerationConstants.room_middle;
                    }
                }
            }


            return floor_map;
        }

        private bool RoomCanBePlaced(int target_room_width, int target_room_height, int upper_left_x, int upper_left_y, int room_padding, char[,] floor_map)
        {
            for (int x = upper_left_x - room_padding; x < upper_left_x + target_room_width + room_padding; x++)
            {
                for (int y = upper_left_y - room_padding; y < upper_left_y + target_room_height + room_padding; y++)
                {
                    if (floor_map[x, y] != ' ') return false;
                }
            }
            return true;
        }

    }
}
