﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.RoomGeneration
{
    interface IRoomGenerator
    {
        char[,] PlaceRooms(char[,] floor_map, FloorGeneratorConfig config, Random random);
    }
}
