﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.Cleanup
{
    interface ICleanupPass
    {
        char[,] Run(ref char[,] floor_map, FloorGeneratorConfig config, Random random);
    }
}
