﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheeseRogue.Utilities.FloorGeneration.Cleanup
{
    public class CleanupDeadends : ICleanupPass 
    {
        Random random;

        public char[,] Run(ref char[,] floor_map, FloorGeneratorConfig config, Random random)
        {
            this.random = random;
            ClearDeadEnds(floor_map, config);
            return floor_map;
        }

        void ClearDeadEnds(char[,] floor_map, FloorGeneratorConfig config)
        {
            int passes = 0;
            bool done = false;
            while (!done && passes < config.deadend_passes)
            {
                done = true;
                for (int x = 0; x < floor_map.GetLength(0); x++)
                {
                    for (int y = 0; y < floor_map.GetLength(1); y++)
                    {
                        if (floor_map[x, y] == LevelGenerationConstants.room_edge && IsDeadEnd(new cell(x, y), floor_map))
                        {
                            done = false;
                            floor_map[x, y] = ' ';
                        }
                    }
                }
                passes++;
            }
        }
        bool IsDeadEnd(cell icell, char[,] floor_map)
        {
            int count = 0;
            if (icell.x > 0 && floor_map[icell.x - 1, icell.y] != ' ')
                count++;
            if (icell.x < floor_map.GetLength(0) - 1 && floor_map[icell.x + 1, icell.y] != ' ')
                count++;
            if (icell.y > 0 && floor_map[icell.x, icell.y - 1] != ' ')
                count++;
            if (icell.y < floor_map.GetLength(1) - 1 && floor_map[icell.x, icell.y + 1] != ' ')
                count++;
            return count <= 1;

        }
    }
}
