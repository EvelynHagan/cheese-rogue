﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogue.Utilities.FloorGeneration.Cleanup
{
    class x2Scale : ICleanupPass
    {
        public char[,] Run(ref char[,] floor_map, FloorGeneratorConfig config, Random random)
        {
            char[,] new_map = new char[floor_map.GetLength(0) * 2, floor_map.GetLength(1) * 2];
            for(int x =0; x < floor_map.GetLength(0); x++)
            {
                for(int y=0; y < floor_map.GetLength(1); y++)
                {
                    new_map[x * 2, y * 2] = floor_map[x, y];
                    new_map[(x * 2)+1, y * 2] = floor_map[x, y];
                    new_map[x * 2, (y * 2)+1] = floor_map[x, y];
                    new_map[(x * 2) + 1, (y * 2) + 1] = floor_map[x, y];
                }
            }
            floor_map = new_map;
            return floor_map;
        }
    }
}