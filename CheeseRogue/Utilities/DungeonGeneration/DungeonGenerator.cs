﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration;

namespace CheeseRogue.Utilities.DungeonGeneration
{
    class DungeonGenerator
    {
        public Dungeon GenerateDungeon(Dungeon dungeon)
        {
            for(int i =0; i < 10; i++)
            {
                dungeon.floors.Add(GenerateFloor());
            }

            return dungeon;
        }

        Floor GenerateFloor()
        {
            FloorGenerator generator = new FloorGenerator();
            FloorGeneratorConfig config = new FloorGeneratorConfig();

            config.deadend_passes = 100;
            config.EmptyAfterHallwayPercent = .02f;
            Random random = new Random();
            config.MapSizeX = random.Next(80, 150);
            config.MapSizeY = random.Next(50, 100);
            config.RoomMinHeight = random.Next(3, 7);
            config.RoomMinWidth = config.RoomMinHeight;
            int maxsize = 10;
            if (random.Next(100) > 75)
                maxsize = 20;
            config.RoomMaxHeight = random.Next(config.RoomMinHeight + 1, maxsize);
            config.RoomMinWidth = config.RoomMaxHeight;
            config.RoomPadding = random.Next(1, 12);
            config.hallway_gen_type = Utilities.FloorGeneration.HallwayGeneration.HallwayTypes.RecursiveBacktrack;
            config.RoomPlacementAttempts = random.Next(200, 3000);

            Floor floor = generator.generate_floor(config);
            return floor;
        }


    }
}
