﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration;

namespace CheeseRogue.Utilities.DungeonGeneration
{
    class Dungeon
    {
        Random random;
        public List<Floor> floors { get; set; }
        public Dungeon()
        {
            floors = new List<Floor>();
        }
    }
}
