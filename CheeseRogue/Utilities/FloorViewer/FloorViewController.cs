﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration;

namespace CheeseRogue.Utilities.FloorViewer
{
    class FloorViewController
    {
        FloorView view;
        Floor floor;
        public FloorViewController()
        {
            FloorGenerator gen = new FloorGenerator();
            floor = gen.generate_floor(new FloorGeneratorConfig());
            view = new FloorView();
            view.x_bound = floor.floor_map.GetLength(0);
            view.y_bound = floor.floor_map.GetLength(1);
        }

        public void Run()
        {
            while (true)
            {
                DrawView();
                if (ReadInput())
                    return;
            }
        }

        bool ReadInput()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            switch (keyInfo.Key)
            {
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                    view.MoveView(Direction.North);
                    break;
                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                    view.MoveView(Direction.South);
                    break;
                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                    view.MoveView(Direction.West);
                    break;
                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                    view.MoveView(Direction.East);
                    break;
                case ConsoleKey.Escape:
                    return true;
            }
            return false;
        }
        void DrawView()
        {
            view.DrawFloor(floor);
        }
    }
}
