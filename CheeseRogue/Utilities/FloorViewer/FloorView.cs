﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogue.Utilities.FloorGeneration;
using Colorful;
using Console = Colorful.Console;
namespace CheeseRogue.Utilities.FloorViewer
{
    class FloorView
    {
        public int width { get; set; }
        public int height { get; set; }

        public int upper_left_x { get; set; }
        public int upper_left_y { get; set; }

        public int x_bound { get; set; }
        public int y_bound { get; set; }

        public FloorView()
        {
            width = 75;
            height = 40;

            upper_left_x = 1;
            upper_left_y = 1;
        }

        public void MoveView(Direction direction)
        {
            cell start = new cell(upper_left_x, upper_left_y);
            cell updated = Directional.GetDirectionalCell(direction, start);
            upper_left_x = Math.Max(0, Math.Min(updated.x, x_bound- width));
            upper_left_y = Math.Max(0, Math.Min(updated.y, y_bound - height));
        }
        

        public void DrawFloor(Floor floor, bool clear = true)
        {
            if (clear)
                Console.Clear();
            StringBuilder stringBuilder = new StringBuilder();
            Formatter[] view = new Formatter[]
            {
                new Formatter("█", Color.Yellow),
            };
            for(int l = 0; l < width+2; l++)
            {
                stringBuilder.Append("{0}");
            }
            stringBuilder.Append("\n");
            for(int y = upper_left_y; y < Math.Min(height+upper_left_y, y_bound); y++)
            {
                stringBuilder.Append("{0}");
                for (int x = upper_left_x; x < Math.Min(width + upper_left_x, x_bound); x++)
                {
                    stringBuilder.Append(floor.floor_map[x, y]);
                }
                stringBuilder.Append("{0}\n");
            }
            for (int l = 0; l < width + 2; l++)
            {
                stringBuilder.Append("{0}");
            }
            stringBuilder.Append("\n");
            Console.WriteFormatted(stringBuilder.ToString(), Color.White, view);
        }
    }
}
