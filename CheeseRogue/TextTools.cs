﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Console = Colorful.Console;
namespace CheeseRogue
{
    class TextTools
    {
        public static string GetCenteredSpace(string input)
        {
            return new String(' ', (Console.WindowWidth - input.Length) / 2);
        }
        public static string Center(string input)
        {
            return new String(' ', (Console.WindowWidth-input.Length)/2) + input;
        }
        public static void WriteCentered(string input)
        {
            Console.WriteLine(Center(input));
        }
        public static void WriteCentered(string input, Color fgcolor)
        {
            Color last = Console.ForegroundColor;
            Console.ForegroundColor = fgcolor;
            WriteCentered(input);
            Console.ForegroundColor = last;
        }
        public static void WriteCentered(string input, Color fgcolor, Color bgColor)
        {
            Color last = Console.BackgroundColor;
            Console.BackgroundColor = bgColor;
            WriteCentered(input, fgcolor);
            Console.BackgroundColor = last;
        }
        public static void WriteBlankLines(int number)
        {
            for (int i = 0; i < number; i++)
                Console.WriteLine("");
        }

        public static void WriteOutTimed(string input, float time_duration, bool Centered = false )
        {
            if (Centered)
                Console.Write(GetCenteredSpace(input));
            for(int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i]);
                System.Threading.Thread.Sleep((int)(time_duration * 1000));
            }
        }
        public static void WriteOutTimed(string input, float time_duration, Color color, bool Centered = false)
        {
            if (Centered)
                Console.Write(GetCenteredSpace(input), color);
            for (int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i], color);
                System.Threading.Thread.Sleep((int)(time_duration * 1000));
            }
        }
        public static string PadWithSpaces(string input, int count)
        {
            return new String(' ', count) + input;
        }

        public static void ClearLastLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.Write(new string(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, Console.CursorTop - 1);
        }
        public static void ClearXLines(int count)
        {
            for (int i = 0; i < count; i++)
                ClearLastLine();
        }
    }
}
