﻿

using System;
using CheeseRogue.Game.World;

namespace CheeseRogue
{
    class Program
    {

        static void Main(string[] args)
        {
            WorldModel world = new WorldModel();
            Console.WindowHeight = 60;
            Console.WindowWidth = 180;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            GameBase base_game = new GameBase();
            base_game.Run();
        }
    }
}
